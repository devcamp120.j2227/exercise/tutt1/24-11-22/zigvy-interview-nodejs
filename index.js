const express = require("express");
const mongoose = require("mongoose");

const app = express();
app.use(express.json());
const port = 8000;

const CommentRouter = require("./app/routers/commentRouter");
const PostRouter = require("./app/routers/postRouter");
const UserRouter = require("./app/routers/userRouter");

const AlbumRouter = require("./app/routers/albumRouter");
const PhotoRouter = require("./app/routers/photoRouter");
const TodoRouter = require("./app/routers/todoRouter");

mongoose.connect("mongodb://localhost:27017/Zigvy_Interview", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB successfully!");
})

app.use("/", UserRouter);
app.use("/", PostRouter);
app.use("/", CommentRouter);
app.use("/", AlbumRouter);
app.use("/", PhotoRouter);
app.use("/", TodoRouter);

app.listen(port, () => {
    console.log("App listening on port ", port)
});