const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const GeoSchema = new Schema({
    lat: {
        type: String,
        required: true
    },
    lng: {
        type: String,
        required: true
    }
});

const AddressSchema = new Schema({
    street:  {
        type: String,
        required: true
    },
    suite:  {
        type: String,
        required: true
    },
    city:  {
        type: String,
        required: true
    },
    zipcode:  {
        type: String,
        required: true
    },
    geo: GeoSchema
})

const CompanySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    catchPhrase: {
        type: String,
        required: true
    },
    bs: {
        type: String,
        required: true
    }
})

const UserSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    address: AddressSchema,
    phone: {
        type: String,
        required: true
    },
    website: {
        type: String,
        require: true
    },
    company: CompanySchema
});

module.exports = mongoose.model('User', UserSchema);