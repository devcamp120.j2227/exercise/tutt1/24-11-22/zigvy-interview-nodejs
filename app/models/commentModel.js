const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CommentSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    postId: {
        type: mongoose.Types.ObjectId,
        ref: "Post"
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Comment", CommentSchema);