const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const AlbumSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    title: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Album", AlbumSchema);