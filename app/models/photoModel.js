const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const PhotoSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: "Album",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    thumbnailUrl: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("Photo", PhotoSchema);