const express = require("express");
const { createAlbum, getAllAlbum, getAlbumById, updateAlbum, deleteAlbum, getAlbumOfUser } = require("../controllers/albumController");
const AlbumRouter = express.Router();

AlbumRouter.post("/albums", createAlbum);
AlbumRouter.get("/albums", getAllAlbum);
AlbumRouter.get("/albums/:albums", getAlbumById);
AlbumRouter.put("/albums/:albums", updateAlbum);
AlbumRouter.delete("/albums/:albums", deleteAlbum);
AlbumRouter.get("/users/:userId/albums", getAlbumOfUser);

module.exports = AlbumRouter;