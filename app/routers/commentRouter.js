const express = require("express");
const { createComment, getAllComment, getCommentById, updateComment, deleteComment, getCommentOfPost } = require("../controllers/commentController");
const CommentRouter = express.Router();

CommentRouter.post("/comments", createComment);
CommentRouter.get("/comments", getAllComment);
CommentRouter.get("/comments/:commentId", getCommentById);
CommentRouter.put("/comments/:commentId", updateComment);
CommentRouter.delete("/comments/:commentId", deleteComment);
CommentRouter.get("/posts/:postId/comments", getCommentOfPost);

module.exports = CommentRouter