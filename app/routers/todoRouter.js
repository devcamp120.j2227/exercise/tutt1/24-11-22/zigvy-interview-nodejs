const express = require("express");
const { createTodo, getAllTodo, getTodoById, updateTodo, deleteTodo, getTodoOfUser } = require("../controllers/todoController");
const TodoRouter = express.Router();

TodoRouter.post("/todos", createTodo);
TodoRouter.get("/todos", getAllTodo);
TodoRouter.get("/todos/:todoId", getTodoById);
TodoRouter.put("/todos/:todoId", updateTodo);
TodoRouter.delete("/todos/:todoId", deleteTodo);
TodoRouter.get("/users/:userId/todos", getTodoOfUser);

module.exports = TodoRouter;