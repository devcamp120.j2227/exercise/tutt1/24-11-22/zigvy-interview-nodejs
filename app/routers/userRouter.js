const express = require("express");
const { createUser, getAllUser, getUserById, updateUser, deleteUser } = require("../controllers/userController");
const UserRouter = express.Router();

UserRouter.post("/users", createUser);
UserRouter.get("/users", getAllUser);
UserRouter.get("/users/:userId", getUserById);
UserRouter.put("/users/:userId", updateUser);
UserRouter.delete("/users/:userId", deleteUser);

module.exports = UserRouter