const express = require("express");
const { createPhoto, getAllPhoto, getPhotoById, updatePhoto, deletePhoto, getPhotoOfAlbum } = require("../controllers/photoController");
const PhotoRouter = express.Router();

PhotoRouter.post("/photos", createPhoto);
PhotoRouter.get("/photos", getAllPhoto);
PhotoRouter.get("/photos/:photoId", getPhotoById);
PhotoRouter.put("/photos/:photoId", updatePhoto);
PhotoRouter.delete("/photos/:photoId", deletePhoto);
PhotoRouter.get("/albums/:albumId/photos", getPhotoOfAlbum);

module.exports = PhotoRouter;