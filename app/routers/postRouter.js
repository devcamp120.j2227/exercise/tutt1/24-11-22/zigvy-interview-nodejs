const express = require("express");
const { createPost, getAllPost, getPostById, updatePost, deletePost, getPostOfUser } = require("../controllers/postController");
const PostRouter = express.Router();

PostRouter.post("/posts", createPost);
PostRouter.get("/posts", getAllPost);
PostRouter.get("/posts/:postId", getPostById);
PostRouter.put("/posts/:postId", updatePost);
PostRouter.delete("/posts/:postId", deletePost);
PostRouter.get("/users/:userId/posts", getPostOfUser);

module.exports = PostRouter;