const mongoose = require("mongoose");
const PhotoModel = require("../models/photoModel");

const createPhoto = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!body.url) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "url is required"
        })
    }
    if(!body.thumbnailUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "thumbnailUrl is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId: body.albumId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    PhotoModel.create(newPhoto, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create photo",
                data: data
            })
        }
    })
}

const getAllPhoto = (req, res) => {
    // B1: Thu thập dữ liệu
    let albumId = req.query.albumId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(albumId) {
        condition.albumId = albumId
    }
    // B3: Xử lý và hiển thị kết quả
    PhotoModel.find(condition)
    .populate("albumId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get all photo",
                data: data
            })
        }
    })
}

const getPhotoById = (req, res) => {
    // B1: Thu thập dữ liệu
    let photoId = req.params.photoId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    PhotoModel.findById(photoId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get photo by id",
                data: data
            })
        }
    })
}

const updatePhoto = (req, res) => {
    // B1: Thu thập dữ liệu
    let photoId = req.params.photoId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!body.url) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "url is required"
        })
    }
    if(!body.thumbnailUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "thumbnailUrl is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updatePhoto = {
        userId: body.userId,
        title: body.title,
        url: body.url,
        thumbnailUrl: body.thumbnailUrl
    }
    PhotoModel.findByIdAndUpdate(photoId, updatePhoto)
    .populate("albumId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update photo",
                data: data
            })
        }
    })
}

const deletePhoto = (req, res) => {
    // B1: Thu thập dữ liệu
    let photoId = req.params.photoId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    PhotoModel.findByIdAndDelete(photoId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete photo"
            })
        }
    })
}

const getPhotoOfAlbum = (req, res) => {
    // B1: Thu thập dữ liệu
    let albumId = req.params.albumId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(albumId) {
        condition.albumId = albumId
    }
    // B3: Xử lý và hiển thị kết quả
    PhotoModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get photo of album",
                data: data
            })
        }
    })
}

module.exports = {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    updatePhoto,
    deletePhoto,
    getPhotoOfAlbum
}