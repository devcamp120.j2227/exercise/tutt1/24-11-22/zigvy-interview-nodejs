const mongoose = require("mongoose");
const AlbumModel = require("../models/albumModel");

const createAlbum = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title
    }
    AlbumModel.create(newAlbum, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create album",
                data: data
            })
        }
    })
}

const getAllAlbum = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.query.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    AlbumModel.find(condition)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get all album",
                data: data
            })
        }
    })
}

const getAlbumById = (req, res) => {
    // B1: Thu thập dữ liệu
    let albumId = req.params.albumId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    AlbumModel.findById(albumId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get album by id",
                data: data
            })
        }
    })
}

const updateAlbum = (req, res) => {
    // B1: Thu thập dữ liệu
    let albumId = req.params.albumId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateAlbum = {
        userId: body.userId,
        title: body.title
    }
    AlbumModel.findByIdAndUpdate(albumId, updateAlbum)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update album",
                data: data
            })
        }
    })
}

const deleteAlbum = (req, res) => {
    // B1: Thu thập dữ liệu
    let albumId = req.params.albumId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    AlbumModel.findByIdAndDelete(albumId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete album"
            })
        }
    })
}

const getAlbumOfUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    AlbumModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get album of user",
                data: data
            })
        }
    })
}

module.exports = {
    createAlbum,
    getAllAlbum,
    getAlbumById,
    updateAlbum,
    deleteAlbum,
    getAlbumOfUser
}