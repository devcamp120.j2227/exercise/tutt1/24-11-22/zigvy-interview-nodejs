const mongoose = require("mongoose");
const TodoModel = require("../models/todoModel");

const createTodo = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newTodo = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        completed: body.completed
    }
    TodoModel.create(newTodo, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create todo",
                data: data
            })
        }
    })
}

const getAllTodo = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.query.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    TodoModel.find(condition)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get all todo",
                data: data
            })
        }
    })
}

const getTodoById = (req, res) => {
    // B1: Thu thập dữ liệu
    let todoId = req.params.todoId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    TodoModel.findById(todoId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get todo by id",
                data: data
            })
        }
    })
}

const updateTodo = (req, res) => {
    // B1: Thu thập dữ liệu
    let todoId = req.params.todoId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateTodo = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        completed: body.completed
    }
    TodoModel.findByIdAndUpdate(todoId, updateTodo)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update todo",
                data: data
            })
        }
    })
}

const deleteTodo = (req, res) => {
    // B1: Thu thập dữ liệu
    let todoId = req.params.todoId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    TodoModel.findByIdAndDelete(todoId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete todo"
            })
        }
    })
}

const getTodoOfUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    TodoModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get todo of user",
                data: data
            })
        }
    })
}

module.exports = {
    createTodo,
    getAllTodo,
    getTodoById,
    updateTodo,
    deleteTodo,
    getTodoOfUser
}