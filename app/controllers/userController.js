const mongoose = require("mongoose");
const UserModel = require("../models/userModel");

const createUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.username) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "username is required"
        })
    }
    if(!body.address) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "address is required"
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if(!body.website) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "website is required"
        })
    }
    if(!body.company) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "company is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        username: body.username,
        email: body.email,
        address: {
            street: body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    UserModel.create(newUser, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create user",
                data: data
            })
        }
    })
}

const getAllUser = (req, res) => {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // B3: Xử lý và hiển thị kết quả
    UserModel.find()
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get all User",
                data: data
            })
        }
    })
}

const getUserById = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    UserModel.findById(userId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get user by id",
                data: data
            })
        }
    })
}

const updateUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.username) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "username is required"
        })
    }
    if(!body.address) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "address is required"
        })
    }
    if(!body.phone) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "phone is required"
        })
    }
    if(!body.website) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "website is required"
        })
    }
    if(!body.company) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "company is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateUser = {
        name: body.name,
        username: body.username,
        email: body.email,
        address: {
            street: body.address.street,
            suite: body.address.suite,
            city: body.address.city,
            zipcode: body.address.zipcode,
            geo: {
                lat: body.address.geo.lat,
                lng: body.address.geo.lng
            }
        },
        phone: body.phone,
        website: body.website,
        company: {
            name: body.company.name,
            catchPhrase: body.company.catchPhrase,
            bs: body.company.bs
        }
    }
    UserModel.findByIdAndUpdate(userId, updateUser)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update user by id",
                data: data
            })
        }
    })
}

const deleteUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    UserModel.findByIdAndDelete(userId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete user"
            })
        }
    })
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUser,
    deleteUser
}