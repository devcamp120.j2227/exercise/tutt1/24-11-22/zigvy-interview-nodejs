const mongoose = require("mongoose");
const CommentModel = require("../models/commentModel");

const createComment = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(body.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!body.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newComment = {
        _id: mongoose.Types.ObjectId(),
        postId: body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    CommentModel.create(newComment, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create comment",
                data: data
            })
        }
    })
}

const getAllComment = (req, res) => {
    // B1: Thu thập dữ liệu
    let postId = req.query.postId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(postId) {
        condition.postId = postId
    }
    // B3: Xử lý và hiển thị kết quả
    CommentModel.find(condition)
    .populate("postId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get all comment",
                data: data
            })
        }
    })
}

const getCommentById = (req, res) => {
    // B1: Thu thập dữ liệu
    let commentId = req.params.commentId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    PostModel.findById(commentId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get comment by id",
                data: data
            })
        }
    })
}

const updateComment = (req, res) => {
    // B1: Thu thập dữ liệu
    let commentId = req.params.commentId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    if(!body.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!body.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!body.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updateComment = {
        postId: body.postId,
        name: body.name,
        email: body.email,
        body: body.body
    }
    CommentModel.findByIdAndUpdate(commentId, updateComment)
    .populate("postId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update comment",
                data: data
            })
        }
    })
}

const deleteComment = (req, res) => {
    // B1: Thu thập dữ liệu
    let commentId = req.params.commentId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    CommentModel.findByIdAndDelete(commentId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete comment"
            })
        }
    })
}

const getCommentOfPost = (req, res) => {
    // B1: Thu thập dữ liệu
    let postId = req.params.postId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(postId) {
        condition.postId = postId
    }
    // B3: Xử lý và hiển thị kết quả
    CommentModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get comment of post",
                data: data
            })
        }
    })
}

module.exports = {
    createComment,
    getAllComment,
    getCommentById,
    updateComment,
    deleteComment,
    getCommentOfPost
}