const mongoose= require("mongoose");
const PostModel = require("../models/postModel");

const createPost = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!body.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let newPost = {
        _id: mongoose.Types.ObjectId(),
        userId: body.userId,
        title: body.title,
        body: body.body
    }
    PostModel.create(newPost, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully create post",
                data: data
            })
        }
    })
}

const getAllPost = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.query.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    PostModel.find(condition)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get all post",
                data: data
            })
        }
    })
}

const getPostById = (req, res) => {
    // B1: Thu thập dữ liệu
    let postId = req.params.postId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    PostModel.findById(postId)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get post by id",
                data: data
            })
        }
    })
}

const updatePost = (req, res) => {
    // B1: Thu thập dữ liệu
    let postId = req.params.postId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(!body.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if(!body.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    let updatePost = {
        userId: body.userId,
        title: body.title,
        body: body.body
    }
    PostModel.findByIdAndUpdate(postId, updatePost)
    .populate("userId")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully update post",
                data: data
            })
        }
    })
}

const deletePost = (req, res) => {
    // B1: Thu thập dữ liệu
    let postId = req.params.postId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    // B3: Xử lý và hiển thị kết quả
    PostModel.findByIdAndDelete(postId, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully delete post"
            })
        }
    })
}

const getPostOfUser = (req, res) => {
    // B1: Thu thập dữ liệu
    let userId = req.params.userId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    // Tạo condition chứa dữ liệu
    let condition = {};
    if(userId) {
        condition.userId = userId
    }
    // B3: Xử lý và hiển thị kết quả
    PostModel.find(condition)
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Successfully get post of user",
                data: data
            })
        }
    })
}

module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePost,
    deletePost,
    getPostOfUser
}